package com.example.controller;


import javax.websocket.server.PathParam;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AssessmentControler {		
	

    @RequestMapping("/hello")
    public String index() {
        return "Greetings from Spring Boot!";
    }
	
	
	/*
	 * check value is positive integer or not
	 * */
	@RequestMapping(value = "/checkInteger/{number}", method = RequestMethod.GET)
	public ResponseEntity<String> positiveInteger(@PathParam(value="number") Integer number) {
		
		String s = null;
		
		 if(number > 0)
	        {
			 s = number+" is a positive number";
	            System.out.println(s);
	        }
	        else if(number < 0)
	        {
	        	 s = number+" is a negative number";
	            System.out.println(s);
	        }
		return new ResponseEntity<String>(s, HttpStatus.OK);
	}
	
	
	
	
	/*
	 * check value is division by 3 and 5
	 * */

	@RequestMapping(value = "/division", method = RequestMethod.POST)
	public ResponseEntity<String> division(@PathVariable Integer n) {	

		String s = null;
		
	boolean divisibleby3 = (n % 3) == 0;
	boolean divisibleby5 = (n % 5) == 0;

	if (divisibleby3 && divisibleby5) {
		s ="fizz buzz";
		System.out.println(s);
	} else if (divisibleby5) {
		s ="buzz";
		System.out.println(s);
	} else if (divisibleby3) {
		s ="fizz";
		System.out.println( s);
	}
	return new ResponseEntity<>(s, HttpStatus.OK);
	

	}
	
	
	
	
	/*
	 * check input validation
	 * */
	
	@RequestMapping(value = "/validation", method = RequestMethod.GET)
	public ResponseEntity<String> validation(@PathVariable Integer i) {
		
		String s = null;		
		
		if (i>0 && i<1000) {
			s = "entered value is an integer value";
			System.out.println(s);
		}
		return new ResponseEntity<String>(s, HttpStatus.OK);
	}
	
	
}